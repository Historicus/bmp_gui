/**************************************************
 * .cpp file and class declaration for button that is used
 * for writing message
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "writebutton.h"

writebutton::writebutton(QWidget *parent)
	: QPushButton(parent)
{

}

writebutton::~writebutton()
{

}

//enable the button if image chosen
void writebutton::opened(){
	setEnabled(true);
}
//disabled the button if close button pressed
void writebutton::closed(){
	setEnabled(false);
}
