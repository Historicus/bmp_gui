/**************************************************
 * Header file for button that is used
 * for reading message
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#ifndef READBUTTON_H
#define READBUTTON_H

#include <QPushButton>

class readbutton : public QPushButton
{
	Q_OBJECT

public:
	readbutton(QWidget *parent);
	~readbutton();
private slots:
	void opened(void);
	void closed(void);
private:
	
};

#endif // READBUTTON_H
