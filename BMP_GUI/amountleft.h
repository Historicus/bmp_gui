/**************************************************
 * Header file for label holding information of 
 * num of bytes that have been written along with limit
 * of num of bytes that can be written
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#ifndef AMOUNTLEFT_H
#define AMOUNTLEFT_H

#include <QLabel>

class amountleft : public QLabel
{
	Q_OBJECT

public:
	amountleft(QWidget *parent);
	~amountleft();
signals:
	void getTextSize(void);
private slots:
	void amt(int);
	void texthasbeenChanged(void);
	void getSize(int);
private:
	int numbytes;
	int amtleft;
};

#endif // AMOUNTLEFT_H
