/**************************************************
 * .cpp file and class declaration for label that holds the
 * bitmap with image loaded
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "bmpviewer.h"

bmpviewer::bmpviewer(QWidget *parent)
	: QLabel(parent)
{

}

bmpviewer::~bmpviewer()
{
	delete bitmap;
}

//creates dialog to select which bitmap to open
//sends signal to enable text and read/write buttons
void bmpviewer::open(){
	filename = QFileDialog::getOpenFileName(this,
		tr("Open pictures"), "", tr("Bitmap Files (*.bmp *.dib)"));
	if (!filename.isEmpty()){
		bitmap = new SteganographyCodex(filename.toStdString());
		setPixmap(filename);
		setScaledContents(true);

		numBytes((((  (bitmap->getHeigth())  *  (bitmap->getWidth()))*3)/8)-4);

		imageChosen();
	}
}

//removes image and sets text if Closed button is pressed
void bmpviewer::closed(){
	setText("Open an Image to write to");
}

//reads in secret message and sends signal to line edit with message
void bmpviewer::read(){
	message = bitmap->getMessage();
	Qmessage = QString::fromStdString(message);

	textViewerText(Qmessage);
}

//takes Qstring and writes message to image
void bmpviewer::write(QString messageEnc){

	bitmap->setMessage(messageEnc.toStdString());
}

//opens dialog box to save image
void bmpviewer::save(){

	filename = QFileDialog::getSaveFileName(this,
		tr("Save picture"), "", tr("Bitmap Files (*.bmp *.dib)"));

	if (!filename.isEmpty()){
		bitmap->writeImage(filename.toStdString());
	}
}
