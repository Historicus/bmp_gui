/**************************************************
 * Header file for main Qt window
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/

#ifndef BMP_GUI_H
#define BMP_GUI_H

#include <QtWidgets/QMainWindow>
#include "ui_bmp_gui.h"
#include <SteganographyCodex.h>
#include <qfiledialog.h>
#include <qgraphicsscene.h>
#include <qgraphicsview.h>

class BMP_GUI : public QMainWindow
{
	Q_OBJECT

public:
	BMP_GUI(QWidget *parent = 0);
	~BMP_GUI();
private:
	Ui::BMP_GUIClass ui;
};

#endif // BMP_GUI_H
