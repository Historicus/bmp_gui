/**************************************************
 * Dr. Boatright's header for Steganography Codex
 * getWidth, getHeight, and BMP_Handler DLL written by Samuel Kenney
****************************************************/
#pragma once

#include <QString>
#include "BMP_Handler.h"
#include <string>

#ifdef _DEBUG
#pragma comment(lib, "BMP_Handler32d.lib")
#else
#pragma comment(lib, "BMP_Handler32.lib")
#endif

class SteganographyCodex {
public:
	explicit SteganographyCodex(const std::string&);
	virtual ~SteganographyCodex(void);

	//perform the bit manipulation to extract a message and return it
	std::string getMessage(void);

	//rewrite the necessary bits to place the message in the raw bytes
	void setMessage(const std::string&);

	//push changes to disk using the supplied filename
	void writeImage(const std::string&);

	//returns width
	int getWidth(void){return width;}

	//returns height
	int getHeigth(void){return height;}
private:
	unsigned char* rawBytes;
	int width;
	int height;
};
