/**************************************************
 * .cpp file and class declaration for label holding information of 
 * num of bytes that have been written along with limit
 * of num of bytes that can be written
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "amountleft.h"

amountleft::amountleft(QWidget *parent)
	: QLabel(parent)
{
	numbytes = 0;
	amtleft = 0;
}

amountleft::~amountleft()
{

}

//sets the label text with num of bytes written and capacity for the num of bytes
void amountleft::amt(int amount){
	numbytes = amount;
	QString num = QString::number(numbytes);
	QString Qamtleft = QString::number(amtleft);

	setText(Qamtleft + "/" + num);
}

//sends signal to Line Edit if the text has been changed to get the size of the message
void amountleft::texthasbeenChanged(void){
	getTextSize();
}

//if the num of bytes written and capacity for the num of bytes are the same, it disabled the label
void amountleft::getSize(int size){
	amtleft = size;
	if (amtleft < numbytes){
		setEnabled(true);
		amt(numbytes);
	} else {
		amt(numbytes);
		setEnabled(false);
	}

}
