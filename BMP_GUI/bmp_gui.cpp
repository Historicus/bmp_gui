/**************************************************
 * .cpp file and class declaration for main Qt window
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "bmp_gui.h"

BMP_GUI::BMP_GUI(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

BMP_GUI::~BMP_GUI()
{

}
