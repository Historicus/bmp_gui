/**************************************************
 * .cpp file and class declaration for button that is used
 * for reading message
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "readbutton.h"

readbutton::readbutton(QWidget *parent)
	: QPushButton(parent)
{

}

readbutton::~readbutton()
{

}

//enable the button if image chosen
void readbutton::opened(){
	setEnabled(true);
}
//disabled the button if close button pressed
void readbutton::closed(){
	setEnabled(false);
}