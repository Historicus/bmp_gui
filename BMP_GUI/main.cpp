/**************************************************
 * .cpp file for main function
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "bmp_gui.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	BMP_GUI w;
	w.show();
	return a.exec();
}
