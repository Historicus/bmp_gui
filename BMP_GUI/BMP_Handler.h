/**************************************************
 * Header file for BMP Dll
 * Has loading and saving functionality of the BMP
 * See .cpp for all code
 * Samuel Kenney 11/3/2016
 * Using BMP Version 3
****************************************************/

#pragma once

#define BMPEXPORTS

#ifdef BMPEXPORTS
#define BMP_API __declspec(dllexport)
#else
#define BMP_API __declspec(dllimport)
#endif

class BMP_Handler {
public:
	//both functions for the DLL
	static BMP_API unsigned char* loadBMP(const char*, int&, int&);

	static BMP_API void saveBMP(const char*, const unsigned char*, int, int);
};