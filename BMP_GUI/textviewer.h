/**************************************************
 * Header file for text edit that is used
 * for displaying message and writing new message
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#ifndef TEXTVIEWER_H
#define TEXTVIEWER_H

#include <QTextEdit>

class textviewer : public QTextEdit
{
	Q_OBJECT

public:
	textviewer(QWidget *parent);
	~textviewer();
signals:
	void sendMessage(QString);
	void returnsize(int);
private slots:
	void opened(void);
	void closed(void);
	void changeText(QString);
	void writeText(void);
	void sendText(void);
private:
	QString message;
	bool read;
};

#endif // TEXTVIEWER_H
