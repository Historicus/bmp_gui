/**************************************************
 * Header file for label that holds the
 * bitmap with image loaded
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#ifndef BMPVIEWER_H
#define BMPVIEWER_H

#include <QLabel>
#include <qfiledialog.h>
#include <string>
#include "SteganographyCodex.h"

class bmpviewer : public QLabel
{
	Q_OBJECT

public:
	bmpviewer(QWidget *parent);
	~bmpviewer();
signals:
	void imageChosen(void);
	void textViewerText(QString);
	void numBytes(int);
private slots:
	void open(void);
	void closed(void);
	void read(void);
	void write(QString);
	void save(void);
private:
	QString filename;
	std::string message;
	QString Qmessage;
	std::string messageEnc;

	SteganographyCodex *bitmap;
};

#endif // BMPVIEWER_H
