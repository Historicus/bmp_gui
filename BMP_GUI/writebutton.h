/**************************************************
 * Header file for button that is used
 * for writing message
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#ifndef WRITEBUTTON_H
#define WRITEBUTTON_H

#include <QPushButton>

class writebutton : public QPushButton
{
	Q_OBJECT

public:
	writebutton(QWidget *parent);
	~writebutton();
private slots:
	void opened(void);
	void closed(void);
private:
	
};

#endif // WRITEBUTTON_H
