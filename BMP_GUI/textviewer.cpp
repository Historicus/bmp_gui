/**************************************************
 * .cpp file and class declaration for text edit that is used
 * for displaying message and writing new message
 * Samuel Kenney 11/29/2016
 * Using BMP Verion 3
****************************************************/
#include "textviewer.h"

textviewer::textviewer(QWidget *parent)
	: QTextEdit(parent)
{
	setText("");
	read = false;
}

textviewer::~textviewer()
{

}
//if image chosen enable and clear message
void textviewer::opened(){
	setEnabled(true);
	clear();
}
//if closed pressed disable box
void textviewer::closed(){
	setEnabled(false);
	clear();
}
//once message is read change text
void textviewer::changeText(QString message){
	read = true;
	this->message = message;
	setText(message);
}

//sends signal with text to write message into image
void textviewer::writeText(void){
	sendMessage(toPlainText());
}

//returns number of characters in message
void textviewer::sendText(){
	returnsize(toPlainText().length());
}
